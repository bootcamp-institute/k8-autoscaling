# create VPA

# simple VPA over [../hpa deployment](../hpa/deployment.yaml)
```
kubectl apply -f vpa.yaml
```


```
kubectl describe vpa php-apache-vpa
```

# VPA Off mode

```
kubectl apply -f vpa-off.yaml

kubectl get pod -l "app=my-off-deployment" --output yaml


kubectl get vpa my-vpa --output yaml
```


# initial
```
kubectl apply -f vpa-auto.yaml

kubectl get pod -l "app=my-auto-deployment" --output yaml


kubectl get vpa my-auto-vpa --output yaml
```


# Initial mode

```
kubectl apply -f vpa-initial.yaml

kubectl get pod -l "app=my-initial-deployment" --output yaml

kubectl get vpa my-initial-vpa --output yaml

# restart deployment
kubectl scale deployment.v1.apps/my-initial-deployment --replicas=0

kubectl scale deployment.v1.apps/my-initial-deployment --replicas=2

# get new recomendations
kubectl get pod -l "app=my-initial-deployment" --output yaml


```