
# HPA

Add HPA to a deployment of your own.

```
git clone git@gitlab.com:bootcamp-institute/kubernetes-scale-that-app.git
```

```
# deploy the app
kubectl kustomize build i | kubectl apply -f -

# watch resources

watch “kubectl top node && kubectl top pod”

# Hit LB

curl http://35.246.194.4:5000
```

## Edit Deployment

```
kubectl edit deployments app
```

![deploy](img/edit.png)


```
curl http://35.246.194.4:5000
```

![result](img/result.png)

We see the limits are in place and working. Nice one Kubernetes!


## Create HPA

```
kubectl autoscale deployment app --cpu-percent=50 --min=3 --max=10
kubectl get hpa



curl http://35.246.194.4:5000
```

A new POD should be created

![new](img/new_pod.png)

## We can reach node limits

![limits](img/limits_reached.png)

Thats were cluster AutoScaling is great, it would be nice if we can have


# Cluster Auto Scaler (CA)

1. Create a cluster with autoscaling enable
2. Repeat the same exercise but in a CA Enabled cluster.



[FAQ](https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/FAQ.md)
```markdown
# FAQ
- Cluster Autoscaler increases the size of the cluster when:
- there are pods that failed to schedule on any of the current nodes due to insufficient resources.
- adding a node similar to the nodes currently present in the cluster would help.
Cluster Autoscaler decreases the size of the cluster when some nodes are consistently unneeded for a significant amount of time. A node is unneeded when it has low utilization and all of its important pods can be moved elsewhere. (source)

# scaling down
- When scaling down, cluster autoscaler respects scheduling and eviction rules set on Pods. These restrictions can prevent a node from being deleted by the autoscaler. A node’s deletion could be prevented if it contains a Pod with any of these conditions:
- The Pod’s affinity or anti-affinity rules prevent rescheduling.
- The Pod has local storage.
- The Pod is not managed by a Controller such as a Deployment, StatefulSet, Job or ReplicaSet. (source)

```


# VPA

Create a different Deployment to add VPA instead of HPA